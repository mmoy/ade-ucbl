# Find the latest version of this program at
# https://gitlab.inria.fr/mmoy/ade-ucbl

# BSD 2-Clause License
#
# Copyright (c) 2017, Matthieu Moy
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import argparse
import datetime
import csv
import re
import os
from types import NoneType
from typing import Callable, Dict, List, Optional, Set, Union
from xmlrpc.client import Boolean

__version__ = '0.1'


def make_parser():
    parser = argparse.ArgumentParser(description="""
    Manage UCBL room booking requests.""")
    parser.add_argument(
        "--output-file", '-o',
        help="Output file.",
        metavar="FILE", type=str)
    parser.add_argument(
        "--version",
        action='store_true',
        default=False,
        help="Print the version of booklib.py")
    parser.add_argument(
        "--csv",
        action='store_true',
        default=False,
        help="Print bookings in CSV format")
    parser.add_argument(
        "--pretty",
        action='store_true',
        default=False,
        help="Print bookings in pretty format (human readable)")
    parser.add_argument(
        "--ics",
        action='store_true',
        default=False,
        help="Print bookings in ics (alias iCalendar) format")
    parser.add_argument(
        "--group-summary",
        action='store_true',
        default=False,
        help="Print per-group summary")
    parser.add_argument(
        "--all-ics",
        action='store_true',
        default=False,
        help="Print bookings in ics, one per teacher.")
    parser.add_argument(
        "--group",
        metavar='GROUP',
        type=str,
        action='append',
        default=[],
        help="Show only bookings for GROUP (repeat for multiple groups)")
    parser.add_argument(
        "--teacher",
        metavar='TEACHER',
        type=str,
        action='append',
        default=[],
        help="Show only bookings for TEACHER (repeat for multiple teachers)")
    parser.add_argument(
        "--detailed",
        action='store_true',
        default=None,
        help="Include detailed comments in CSV output (does not fit in the import/export format of the UCBL form)")
    parser.add_argument(
        "--no-detailed",
        action='store_false',
        dest='detailed',
        help="Disable detailed comment in --pretty format")
    return parser


def die(*msg):
    # Don't mix stdout and stderr
    sys.stdout.flush()
    print(*msg, file=sys.stderr)
    sys.exit(1)


def parse_cli():
    parser = make_parser()

    args = parser.parse_args()

    outputs = sum((bool(x) for x in (
        args.ics, args.all_ics, args.csv, args.group_summary, args.pretty)))
    if outputs > 1:
        print("Options --ics, --all-ics, --csv and --group-summary are incompatible.")
        sys.exit(1)

    if outputs == 0:
        args.csv = True

    if args.all_ics and args.output_file:
        print("Options --all-ics and -o, --output-file are incompatible.")
        sys.exit(1)

    if args.version:
        print("booklib.py version", __version__)
        sys.exit(0)

    return args


def set_year(year):
    global current_year
    current_year = year


def set_week(week):
    global current_week
    current_week = week


current_day_function: Callable


def set_day(day_function: Callable):
    global current_day_function
    current_day_function = day_function


MORNING = "morning"
AFTERNOON = "afternoon"


def set_half_day(half_day):
    global current_half_day
    current_half_day = half_day


def h(time: str):
    return current_day_function(time)


def slot(n):
    return h({
        MORNING: {
            1: '8:00',
            2: '9:45',
            3: '11:30'
        },
        AFTERNOON: {
            1: '14:00',
            2: '15:45',
            3: '17:30'
        }
    }[current_half_day][n])


current_course : 'Course'


def set_course(course):
    global current_course
    current_course = course


def mk_time(day_of_week, hour, year=None, week=None):
    if year is None:
        year = current_year
    if week is None:
        week = current_week
    # Accept '9h45' instead of '9:45'
    m = re.match(r'([0-9]+)h([0-9]+)$', hour)
    if m:
        hour = '{}:{}'.format(m.group(1), m.group(2))
    return datetime.datetime.strptime(
        '{} {} {} {}'.format(year, week, day_of_week, hour),
        '%Y %W %w %H:%M')


def _day_function(day_of_week):
    def f(*args, **kwargs):
        return mk_time(day_of_week, *args, **kwargs)
    return f


monday = _day_function(1)
tuesday = _day_function(2)
wednesday = _day_function(3)
thursday = _day_function(4)
friday = _day_function(5)


class Teacher:
    everybody: Set['Teacher'] = set()

    def __init__(self, name: str):
        self.name = name
        Teacher.everybody.add(self)

    def __repr__(self):
        return self.name


class Group:
    groups: Dict[str, Set['Group']] = dict()

    def __init__(self, kind: str, name: str, teachers: Union[Teacher, Set[Teacher]],
                 nb_students: Optional[int] = None):
        self.kind = kind
        self.name = name
        self.nb_students = nb_students
        if isinstance(teachers, Teacher):
            self.teachers = set((teachers,))
        else:
            self.teachers = teachers
        if kind not in Group.groups:
            Group.groups[kind] = set()
        for gs in Group.groups.values():
            if name in (g.name for g in gs):
                raise Exception(f"A group named {name} already exists")
        Group.groups[kind].add(self)

    def teacher(self) -> Teacher:
        if len(self.teachers) != 1:
            die("Cannot use .teacher() if the group doesn't have a unique teacher")
        return next(iter(self.teachers))

    def teachers_str(self) -> str:
        return "; ".join(str(x) for x in self.teachers)

    def __repr__(self):
        return '{} ({}, {})'.format(self.name,
                                    self.kind,
                                    self.teachers_str())

    def __lt__(self, other):
        return self.name < other.name


BANALISE = 1
INFORMATIQUE = 2
AUTRE = 3
CM = 'CM'
TP = 'TP'
TD = 'TD'
TPR = {
    'roomtype': AUTRE,
    'precise_comment': "Salles TP réseaux (Nautibus TPR1, TPR2, TPR3)"
}

DURATION_1X = datetime.timedelta(hours=1, minutes=30)
DURATION_2X = datetime.timedelta(hours=3, minutes=15)

DURATION_1_SLOT = {
    'duration': DURATION_1X,
}
DURATION_1_SLOT_PLUS = {
    'duration': DURATION_2X,
    'official_duration': DURATION_1X
}
DURATION_2_SLOT = {
    'duration': DURATION_2X,
    'official_duration': datetime.timedelta(hours=3, minutes=0)
}


class Booking:
    teachers: Set[Teacher]

    def __init__(self,
                 group: Group,
                 date: datetime.datetime,
                 duration: datetime.timedelta = DURATION_1X,
                 official_duration: Optional[datetime.timedelta] = None,
                 comment: str = "",
                 kind: Optional[str] = None,
                 teacher: Optional[Teacher] = None,
                 nb_students: Optional[int] = None,
                 teachers: Optional[Set[Teacher]] = None,
                 roomtype=None,
                 precise_comment: str = "",
                 handicap: int = 0,
                 detailed_comment: str = ""):
        self.group = group
        self.date = date
        self.duration = duration
        if official_duration is None:
            official_duration = duration
        self.official_duration = official_duration
        self.comment = comment
        self.detailed_comment = detailed_comment
        self.kind = kind
        if teacher is not None:
            assert teachers is None
            teachers = set((teacher,))
        self.nb_students = nb_students
        self.roomtype = roomtype
        self.precise_comment = precise_comment
        self.handicap = handicap

        if self.roomtype is None:
            if self.kind == TP:
                self.roomtype = INFORMATIQUE
            else:
                self.roomtype = BANALISE

        if self.kind is None:
            self.kind = group.kind

        if teachers is None:
            assert group.teachers is not None
            teachers = group.teachers
        self.teachers = set(teachers)

        if self.nb_students is None:
            self.nb_students = group.nb_students

    def teacher(self) -> Teacher:
        if self.teachers is not None and len(self.teachers) != 1:
            die("Cannot use .teacher() if the booking doesn't have a unique teacher")
        assert self.teachers is not None
        return next(iter(self.teachers))

    def teachers_str(self) -> str:
        return "; ".join(str(x) for x in self.teachers)

    def __lt__(self, a) -> Boolean:
        if self.date > a.date:
            return False
        if self.group > a.group:
            return False
        return True


class Course:
    bookings: List[Booking] = []

    def __init__(self,
                 composante: str,
                 etape: str,
                 ue: str,
                 sequence: int,
                 responsable: str,
                 comment: str):
        self.composante = composante
        self.etape = etape
        self.ue = ue
        self.sequence = sequence
        self.responsable = responsable
        self.comment = comment

    def add_booking(self, *args, **kwargs):
        self.bookings.append(Booking(*args, **kwargs))

    def filter(self, args) -> List[Booking]:
        bookings = []
        for b in self.bookings:
            if args.group and not b.group.name in args.group:
                continue
            if args.teacher:
                skip = True
                for t in b.teachers:
                    if t.name in args.teacher:
                        skip = False
                if skip:
                    continue
            bookings.append(b)
        return bookings

    def open(self, args, binary, **kwargs):
        if args.output_file:
            return open(args.output_file, 'wb' if binary else 'w', **kwargs)
        else:
            return sys.stdout.buffer if binary else sys.stdout

    def csv(self, args):
        with self.open(args, False, newline='') as csvfile:
            out = csv.writer(
                csvfile,
                quoting=csv.QUOTE_MINIMAL)
            out.writerow(["La formation"])
            out.writerow(["Code composante", "Code étape", "Code UE",
                          "Séquence", "Nom responsable", "Commentaire"])
            out.writerow([self.composante, self.etape, self.ue,
                          self.sequence, self.responsable, self.comment])
            out.writerow([])
            out.writerow(["Les créneaux"])
            out.writerow(["Date Deb", "Répéter x fois", "Heure Deb",
                          "Date Fin (pas utilisée à l'import)", "Heure Fin",
                          "Commentaire", "Ressource", "Groupe", "Effectif groupe",
                          "Intervenant",
                          "type salle", "Précision", "Accès handicapé"])
            for b in self.filter(args):
                line = [
                    b.date.strftime('%d/%m/%Y'),
                    '1',
                    b.date.strftime('%H:%M'),
                    b.date.strftime('%d/%m/%Y'),
                    (b.date + b.duration).strftime('%H:%M'),
                    b.comment,
                    b.kind,
                    b.group.name,
                    b.nb_students,
                    b.teachers_str(),
                    b.roomtype,
                    b.precise_comment,
                    b.handicap
                ]
                if args.detailed and b.detailed_comment:
                    line.append(b.detailed_comment)
                out.writerow(line)

    def pretty(self, args):
        prev_date = None
        with self.open(args, False, newline='') as outfile:
            for b in sorted(self.filter(args)):
                if b.date.date() != prev_date:
                    outfile.write("{}{}:{}".format(
                        os.linesep,
                        b.date.strftime('%d/%m/%Y'),
                        os.linesep))
                    prev_date = b.date.date()
                outfile.write(
                    "  {} -> {} : {} ({}, {}{}){}".format(
                        b.date.strftime('%H:%M'),
                        (b.date + b.duration).strftime('%H:%M'),
                        b.comment,
                        b.group.name,
                        b.teachers_str(),
                        " : " + b.detailed_comment if b.detailed_comment and args.detailed is not False else "",
                        os.linesep
                    ))

    def ics(self, args):
        import pytz
        import icalendar  # type: ignore

        def copy_date(date):
            return datetime.datetime(date.year, date.month, date.day, date.hour, date.minute,
                                     tzinfo=pytz.timezone('Europe/Paris'))

        cal = icalendar.Calendar()
        cal.add('prodid', '-//My calendar product//mxm.dk//')
        cal.add('version', '2.0')
        for b in self.filter(args):
            event = icalendar.Event()
            summary = '{} (Groupe {})'.format(b.comment, b.group)
            description = ""
            if self.comment:
                description += "{} ({})\n".format(self.comment, self.ue)
            else:
                description += "{}\n".format(self.ue)
            description += ', '.join(str(x) for x in b.teachers)
            if b.detailed_comment:
                description += "\n" + b.detailed_comment

            event.add('summary', summary)
            dtstart = copy_date(b.date)
            dtend = copy_date(b.date + b.duration)
            event.add('dtstart', dtstart)
            event.add('dtend', dtend)
            event.add('dtstamp', datetime.datetime.now())
            event.add('description', description)
            cal.add_component(event)

        with self.open(args, True) as ical_file:
            ical_file.write(cal.to_ical())

    def per_group_summary(self, args):
        summary = dict()
        for b in self.filter(args):
            if b.group.name not in summary:
                summary[b.group.name] = dict()
            if b.kind not in summary[b.group.name]:
                summary[b.group.name][b.kind] = datetime.timedelta(
                    hours=0, minutes=0)
            summary[b.group.name][b.kind] += b.official_duration
        with self.open(args, False) as summary_file:
            for kind in (TD, TP):
                if kind in Group.groups and Group.groups[kind]:
                    summary_file.write("**{}**{}".format(kind, os.linesep))
                    for g in sorted(Group.groups[kind]):
                        summary_file.write("{}: {}{}".format(
                            g.name,
                            g.teachers_str(),
                            os.linesep))
                    summary_file.write(os.linesep)
            summary_file.write("     ")
            for t in ('CM', 'TP', 'TD', 'Exam'):
                summary_file.write("{:>8}".format(t))
            summary_file.write("\n")
            for k, v in sorted(summary.items()):
                summary_file.write("{:>4}:".format(str(k)))
                for t in ('CM', 'TP', 'TD', 'Exam'):
                    if t in v:
                        # No strftime for timedelta, do it ourselves
                        # to show hh:mm
                        summary_file.write("{:>8}".format(
                            "{:>2d}:{:02d}".format(
                                v[t].seconds // 3600,
                                v[t].seconds // 60 % 60)))
                    else:
                        summary_file.write("{:>8}".format("-"))
                summary_file.write("\n")


def add_booking(*args, **kwargs):
    current_course.add_booking(*args, **kwargs)


def main():
    args = parse_cli()

    if args.all_ics:
        for t in Teacher.everybody:
            args.output_file = t.name + ".ics"
            args.teacher = [t.name]
            current_course.ics(args)
            print("Wrote", args.output_file)
    if args.csv:
        current_course.csv(args)
    if args.pretty:
        current_course.pretty(args)
    if args.ics:
        current_course.ics(args)
    if args.group_summary:
        current_course.per_group_summary(args)
