# ade-ucbl: réservation de salles semi-automatiques avec le système de l'UCBL

Tout ce que vous avez toujours voulu savoir sur la planification des
UE : http://perso.univ-lyon1.fr/thierry.excoffier/planification.html

Site de réservation ``Cellule ADE'' : https://celluleade.univ-lyon1.fr/

Cet outil permet de réduire le travail manuel de soumission à l'outil
``Cellule ADE''. Au lieu de remplir à la main via l'interface
web, les étapes sont :

* Saisie des créneaux sous la forme d'un script Python

* Export via `./script.py --csv -o creneaux.csv`

* Import du `creneaux.csv` dans un tableur. Attention, par défaut,
  LibreOffice transforme les heures `8:00` en `8:00:00`, et
  ``Cellule ADE'' n'aime pas du tout ça. **Bien décocher la
  case « detect special numbers » à l'import CSV.**

* Création d'une nouvelle demande et import CSV depuis
  https://celluleade.univ-lyon1.fr/.

# Les autres fonctionalités sympa

* Résumé groupe par groupe et par type (CM, TD, TP, Exam) :

```
$ ./book_example.py --group-summary
**TD**
A: Bob B.
B: Alice A.
C: Charlie C.
D: David D.

**TP**
A1: Bob B.
A2: Foo Bar
B1: Alice A.
B2: John Smith
C1: Charlie C.
C2: You
D1: David D.
D2: Me

           CM      TP      TD    Exam
  CM:    6:00       -    1:30    1:30
   B:       -       -    7:30       -
   A:       -       -    7:30       -
   D:       -       -    7:30       -
   C:       -       -    7:30       -
  B2:       -   15:00       -       -
  A1:       -   15:00       -       -
  C2:       -   15:00       -       -
  B1:       -   15:00       -       -
  D2:       -   15:00       -       -
  C1:       -   15:00       -       -
  D1:       -   15:00       -       -
  A2:       -   15:00       -       -
```

* Export calendrier global :

```
$ ./book_example.py --ics | head
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//My calendar product//mxm.dk//
BEGIN:VEVENT
SUMMARY:CM 1 (Groupe CM (CM\, Alice A.))
DTSTART;TZID=Europe/Paris;VALUE=DATE-TIME:20200121T080000
DTEND;TZID=Europe/Paris;VALUE=DATE-TIME:20200121T093000
DTSTAMP;VALUE=DATE-TIME:20191203T164101Z
DESCRIPTION:Alice A.
END:VEVENT
...
```

* Export calendrier enseignant par enseignant :

```
$ ./book_example.py --all-ics   
Wrote Foo Bar.ics
Wrote Alice A..ics
Wrote You.ics
Wrote Me.ics
Wrote Bob B..ics
Wrote John Smith.ics
Wrote Charlie C..ics
Wrote David D..ics
```

* Pretty-print texte :

```
$ ./book_example.py --pretty

21/01/2020:
  08:00 -> 09:30 : CM 1 (CM, Alice A.)

28/01/2020:
  08:00 -> 09:30 : TD 1 (B, Alice A.)
  08:00 -> 09:30 : TD 1 (A, Bob B.)
  08:00 -> 09:30 : TD 1 (D, David D.)
  08:00 -> 09:30 : TD 1 (C, Charlie C.)
  09:45 -> 11:15 : TP 1 (B2, John Smith)
  09:45 -> 11:15 : TP 1 (A1, Bob B.)
  09:45 -> 11:15 : TP 1 (C2, You)
  09:45 -> 11:15 : TP 1 (B1, Alice A.)
  09:45 -> 11:15 : TP 1 (D2, Me)
  09:45 -> 11:15 : TP 1 (C1, Charlie C.)
  09:45 -> 11:15 : TP 1 (D1, David D.)
  09:45 -> 11:15 : TP 1 (A2, Foo Bar)

30/01/2020:
  14:00 -> 15:30 : CM 2 (CM, Alice A.)
  ...
```

* Exemple de combinaison d'options : pretty-print et filtre sur un groupe :

```
$ ./book_example.py --pretty --group A

28/01/2020:
  08:00 -> 09:30 : TD 1 (A, Bob B.)

13/02/2020:
  14:00 -> 15:30 : TD 2 (A, Bob B.)

25/02/2020:
  11:30 -> 13:00 : TD 3 (A, Bob B.)

17/03/2020:
  08:00 -> 09:30 : TD 4 (A, Bob B.)

07/04/2020:
  09:45 -> 11:15 : TD 6 (A, Bob B.)
```
# Documentation

* Voir l'exemple dans ce dépôt

* Me demander ;-)
