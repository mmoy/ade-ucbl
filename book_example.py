#!/usr/bin/env python3

from booklib import *

set_course(Course("D64",  # Département informatique
                  "LIF301",  # L3 info parcours info
                  "INF3049L",  # Prog concurrente
                  3,  # Séquence
                  "John Doe",  # Responsable
                  ""))  # Comment

alice = Teacher('Alice A.')
bob = Teacher('Bob B.')
charlie = Teacher('Charlie C.')
david = Teacher('David D.')

foo = Teacher('Foo Bar')
smith = Teacher('John Smith')
you = Teacher('You')
me = Teacher('Me')

ALL = Group('CM', 'CM', alice)
A = Group('TD', 'A', bob)
B = Group('TD', 'B', alice)
C = Group('TD', 'C', charlie)
D = Group('TD', 'D', david)

A1 = Group('TP', 'A1', bob)
B1 = Group('TP', 'B1', alice)
C1 = Group('TP', 'C1', charlie)
D1 = Group('TP', 'D1', david)

A2 = Group('TP', 'A2', foo)
B2 = Group('TP', 'B2', smith)
C2 = Group('TP', 'C2', you)
D2 = Group('TP', 'D2', me)

set_year(2020)

set_week(3)
# Comment is exported to the ADE form. Detailed_comment is just for you, and optional.
add_booking(ALL, tuesday('8:00'), comment="CM 1", detailed_comment="Introduction of the course")

set_week(4)
for g in Group.groups[TD]:
    add_booking(g, tuesday('8:00'), comment="TD 1")

for g in Group.groups[TP]:
    add_booking(g, tuesday('9:45'), comment="TP 1")

add_booking(ALL, thursday('14:00'), comment="CM 2")

set_week(5)
for g in Group.groups[TP]:
    add_booking(g, tuesday('9:45'), comment="TP 2",
                **DURATION_2_SLOT)

set_week(6)
# All groups except one
for g in Group.groups[TD] - {A}:
    add_booking(g, tuesday('9:45'), comment="TD 2")
# Special-case for this group
for g in {A}:
    add_booking(g, thursday('14:00'), comment="TD 2")

set_week(7)
for g in Group.groups[TP]:
    t_slot = thursday('14:00') if g.teacher() in (alice, david, foo) else tuesday('9:45')
    add_booking(g, t_slot,
                **DURATION_2_SLOT,
                comment="TP 3")

set_week(8)
add_booking(ALL, tuesday('9:45'), comment="CM 3")
for g in Group.groups[TD]:
    add_booking(g, tuesday('11:30'), comment="TD 3")

set_week(10)
t_slot = dict()
for g in {A1, B1, C1}:
    t_slot[g] = tuesday('8:00')
for g in {D1, A2, B2}:
    t_slot[g] = tuesday('9:45')
for g in {C2, D2}:
    t_slot[g] = tuesday('11:30')
for g in Group.groups[TP]:
    # You may specify teachers directly on a booking
    add_booking(g, t_slot[g], comment="TP 4 (noté)",
                teachers=(alice, david, foo),
                **TPR)

set_week(11)
# To make it easier to move a complete half-day, you can set day and half-day
# first, and then use slot(1) (= 8:00 or 14:00), slot(2) (= 9:45 or 15:45), ...
# Just change set_day/set_half_day as needed, and keep the schedule identical.
set_day(tuesday)
set_half_day(MORNING)
for g in Group.groups[TD]:
    add_booking(g, slot(1), comment="TD 4")
for g in Group.groups[TP]:
    add_booking(g, slot(2), comment="TP 5", **DURATION_2_SLOT)

set_week(12)
add_booking(ALL, tuesday('9:45'), comment="CCI : amphi mode examen pour 120 étudiants",
            **DURATION_1_SLOT_PLUS,
            kind='TD')
add_booking(ALL, thursday('14:00'), comment="CM 4")

set_week(13)
for g in Group.groups[TP]:
    add_booking(g, tuesday('9:45'), comment="TP 6",
                **DURATION_2_SLOT)

set_week(14)
for g in Group.groups[TD]:
    t_slot = thursday('14:00') if g.teacher() == alice else tuesday('9:45')
    add_booking(g, t_slot, comment="TD 6")

# set_week(15)
# for g in Group.groups[TD]:
#     add_booking(g, tuesday('9:45'), comment="TD 6")

set_week(16)
add_booking(ALL, tuesday('9:45'), comment="Examen : amphi mode examen pour 120 étudiants",
            **DURATION_1_SLOT_PLUS,
            kind='Exam')
main()
